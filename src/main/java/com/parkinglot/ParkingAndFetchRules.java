package com.parkinglot;

public interface ParkingAndFetchRules {

    Ticket park(Car car);

    Car fetch(Ticket ticket);

    Boolean isPark();

    Boolean isFetch(Ticket ticket);

}
