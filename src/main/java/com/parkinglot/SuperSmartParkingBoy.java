package com.parkinglot;

import java.util.List;
import java.util.stream.Collectors;

public class SuperSmartParkingBoy implements ParkingAndFetchRules {

    List<ParkingLot> parkingLots;

    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    @Override
    public Ticket park(Car car) {
        return parkingLots.stream()
                .reduce((left,right)->{
                    if(left.perWithNullPosition()>right.perWithNullPosition())
                        return left;
                    return right;
                })
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePosition::new);
    }

    @Override
    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isFetch(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedTicketException::new);
    }

    @Override
    public Boolean isPark() {
        return ! parkingLots.stream()
                .map(parkingLot -> parkingLot.isPark())
                .collect(Collectors.toList()).isEmpty();
    }

    @Override
    public Boolean isFetch(Ticket ticket) {
        return ! parkingLots.stream()
                .map(parkingLot -> parkingLot.isFetch(ticket))
                .collect(Collectors.toList()).isEmpty();
    }
}
