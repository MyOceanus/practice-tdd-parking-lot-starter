package com.parkinglot;

import java.util.List;

public class ManagerParkingBoy implements ParkingAndFetchRules{



    private List<ParkingLot> parkingLots;

    private List<ParkingAndFetchRules> parkingBoys;

    public ManagerParkingBoy(List<ParkingLot> parkingLots,List<ParkingAndFetchRules> parkingBoys) {
        this.parkingBoys = parkingBoys;
        this.parkingLots = parkingLots;
    }


    @Override
    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(ParkingLot::isPark)
                .findFirst()
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePosition :: new);
    }

    @Override
    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isFetch(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedTicketException::new);
    }

    @Override
    public Boolean isPark() {
        return null;
    }

    @Override
    public Boolean isFetch(Ticket ticket) {
        return null;
    }

    public Ticket useBoyToParking(Car car){
        return parkingBoys.stream()
                .filter(ParkingAndFetchRules::isPark)
                .findFirst()
                .map(parkingAndFetchRules -> parkingAndFetchRules.park(car))
                .orElseThrow(NoAvailablePosition :: new);
    }

    public Car useBoyToFetch(Ticket ticket){
        return parkingBoys.stream()
                .filter(parkingAndFetchRules -> parkingAndFetchRules.isFetch(ticket))
                .findFirst()
                .map(parkingAndFetchRules -> parkingAndFetchRules.fetch(ticket))
                .orElseThrow(UnrecognizedTicketException::new);

    }
}
