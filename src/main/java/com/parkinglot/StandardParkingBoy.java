package com.parkinglot;

import java.util.List;
import java.util.stream.Collectors;

public class StandardParkingBoy implements ParkingAndFetchRules{

    private List<ParkingLot> parkingLots;

    public StandardParkingBoy(List<ParkingLot> parkingLots){
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(ParkingLot::isPark)
                .findFirst()
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePosition :: new);
    }

    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isFetch(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedTicketException::new);
    }

    @Override
    public Boolean isPark() {
        return ! parkingLots.stream()
                .map(parkingLot -> parkingLot.isPark())
                .collect(Collectors.toList()).isEmpty();
    }

    @Override
    public Boolean isFetch(Ticket ticket) {
        return ! parkingLots.stream()
                .map(parkingLot -> parkingLot.isFetch(ticket))
                .collect(Collectors.toList()).isEmpty();
    }
}
