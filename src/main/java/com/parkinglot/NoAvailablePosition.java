package com.parkinglot;

public class NoAvailablePosition extends RuntimeException{
    public NoAvailablePosition() {
        super("no available position");
    }
}
