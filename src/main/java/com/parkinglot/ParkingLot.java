package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {



    private  Integer maxPosition = 2;

    private Map<Ticket,Car> carTicketMap = new HashMap<>();

    public ParkingLot(Integer maxPosition) {
        this.maxPosition = maxPosition;
    }

    public Boolean isPark(){
        return carTicketMap.size() < maxPosition;
    }

    public Ticket park(Car car) {
        if(!isPark()){
            throw new NoAvailablePosition();
        }
        Ticket ticket = new Ticket();
        carTicketMap.put(ticket, car);
        return ticket;
    }

    public Boolean isFetch(Ticket ticket){
        return carTicketMap.containsKey(ticket);
    }

    public Car fetch(Ticket ticket) {

        if (!isFetch(ticket)){
           throw new UnrecognizedTicketException();
        }
        return carTicketMap.remove(ticket);
    }

    public int capacityPosition(){
        return maxPosition - carTicketMap.size();
    }

    public Double perWithNullPosition(){
        return (maxPosition - carTicketMap.size()) /(double) maxPosition;
    }

}
