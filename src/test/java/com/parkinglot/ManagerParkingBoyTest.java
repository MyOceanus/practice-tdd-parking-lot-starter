package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManagerParkingBoyTest {
    @Test
    void should_return_a_ticket_when_park_given_a_parking_lot_and_a_car() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(2);
        parkingLots.add(parkingLot1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        parkingLots.add(parkingLot2);

        List<ParkingAndFetchRules> managerParkingBoys = new ArrayList<>();

        ManagerParkingBoy managerParkingBoy = new ManagerParkingBoy(parkingLots,managerParkingBoys);

        //when
        Ticket ticket = managerParkingBoy.park(car);

        //then
        Assertions.assertTrue(()->parkingLot1.isFetch(ticket));
    }

    @Test
    void should_return_car_when_fetch_given_a_parking_lot_and_a_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));

        List<ParkingAndFetchRules> managerParkingBoys = new ArrayList<>();
        ManagerParkingBoy managerParkingBoy = new ManagerParkingBoy(parkingLots,managerParkingBoys);
        Car car = new Car();
        Ticket ticket = managerParkingBoy.park(car);

        //when
        Car fetchedCar = managerParkingBoy.fetch(ticket);

        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_car_twice_when_fetch_given_a_parking_lot_and_tickets() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        List<ParkingAndFetchRules> managerParkingBoys = new ArrayList<>();
        ManagerParkingBoy managerParkingBoy = new ManagerParkingBoy(parkingLots,managerParkingBoys);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = managerParkingBoy.park(car1);
        Ticket ticket2 = managerParkingBoy.park(car2);
        //when
        Car fetchedCar1 = managerParkingBoy.fetch(ticket1);
        Car fetchedCar2 = managerParkingBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1,fetchedCar1);
        Assertions.assertEquals(car2,fetchedCar2);
    }
    @Test
    void should_throw_exception_when_fetch_given_a_parking_lot_and_a_wrong_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        List<ParkingAndFetchRules> managerParkingBoys = new ArrayList<>();
        ManagerParkingBoy managerParkingBoy = new ManagerParkingBoy(parkingLots,managerParkingBoys);
        Car car = new Car();
        managerParkingBoy.park(car);

        //when
        //then
        UnrecognizedTicketException unrecognizedTicketException  =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                () -> managerParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket",unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_a_parking_lot_and_a_used_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        List<ParkingAndFetchRules> managerParkingBoys = new ArrayList<>();
        ManagerParkingBoy managerParkingBoy = new ManagerParkingBoy(parkingLots,managerParkingBoys );
        Car car = new Car();
        Ticket ticket = managerParkingBoy.park(car);
        Car fetchedCar = managerParkingBoy.fetch(ticket);

        //when
        UnrecognizedTicketException unrecognizedTicketException  =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> managerParkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket",unrecognizedTicketException.getMessage());
    }

    @Test
    void should_park_second_park_when_park_given_a_parking_lot_and_a_car_and_first_no_position() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        ParkingLot parkingLot = new ParkingLot(2);
        parkingLots.add(parkingLot);
        List<ParkingAndFetchRules> managerParkingBoys = new ArrayList<>();
        ManagerParkingBoy managerParkingBoy = new ManagerParkingBoy(parkingLots,managerParkingBoys);
        managerParkingBoy.park(new Car());
        managerParkingBoy.park(new Car());

        //when
        Ticket ticket = managerParkingBoy.park(new Car());


        //then
        Assertions.assertTrue(()->parkingLot.isFetch(ticket));

    }

    @Test
    void should_throw_exception_when_park_given_a_parking_lot_and_a_car_and_no_position() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        List<ParkingAndFetchRules> managerParkingBoys = new ArrayList<>();
        ManagerParkingBoy managerParkingBoy = new ManagerParkingBoy(parkingLots,managerParkingBoys);

        managerParkingBoy.park(new Car());
        managerParkingBoy.park(new Car());
        managerParkingBoy.park(new Car());
        managerParkingBoy.park(new Car());

        //when
        NoAvailablePosition noAvailablePosition  =
                Assertions.assertThrows(NoAvailablePosition.class,
                        () -> managerParkingBoy.park(new Car()));
        Assertions.assertEquals("no available position",noAvailablePosition.getMessage());
    }

    @Test
    void should_return_when_park_given_a_parking_lot_and_a_car_and_use_boy() {
        //given
        List<ParkingLot> managerParkingLot = new ArrayList<>();

        List<ParkingLot> parkingLots1 = new ArrayList<>();
        ParkingLot parkingLot1A = new ParkingLot(2);
        parkingLots1.add(parkingLot1A);
        ParkingLot parkingLot1B = new ParkingLot(2);
        parkingLots1.add(parkingLot1B);

        List<ParkingLot> parkingLots2 = new ArrayList<>();
        ParkingLot parkingLot2A = new ParkingLot(2);
        parkingLots2.add(parkingLot2A);
        ParkingLot parkingLot2B = new ParkingLot(2);
        parkingLots2.add(parkingLot2B);

        List<ParkingAndFetchRules> managerParkingBoys = new ArrayList<>();

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots1);

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots2);

        managerParkingBoys.add(smartParkingBoy);
        managerParkingBoys.add(superSmartParkingBoy);

        ManagerParkingBoy managerParkingBoy = new ManagerParkingBoy(managerParkingLot,managerParkingBoys);


        //when
        Ticket ticket = managerParkingBoy.useBoyToParking(new Car());
        managerParkingBoy.useBoyToFetch(ticket);


        //then
    }

}
