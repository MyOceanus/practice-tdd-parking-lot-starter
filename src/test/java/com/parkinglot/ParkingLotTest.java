package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_a_ticket_when_park_given_a_parking_lot_and_a_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car = new Car();

        //when
        Ticket ticket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_a_parking_lot_and_a_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        //when
        Car fetchedCar = parkingLot.fetch(ticket);

        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_car_twice_when_fetch_given_a_parking_lot_and_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);
        //when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        //then
        Assertions.assertEquals(car1,fetchedCar1);
        Assertions.assertEquals(car2,fetchedCar2);
    }
    @Test
    void should_throw_exception_when_fetch_given_a_parking_lot_and_a_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car = new Car();
        parkingLot.park(car);

        //when
        //then
        UnrecognizedTicketException unrecognizedTicketException  =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                () -> parkingLot.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket",unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_a_parking_lot_and_a_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);

        //when
        UnrecognizedTicketException unrecognizedTicketException  =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> parkingLot.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket",unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_when_park_given_a_parking_lot_and_a_car_and_no_position() {
        //given
        ParkingLot parkingLot = new ParkingLot(4);

        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());

        //when
        NoAvailablePosition noAvailablePosition  =
                Assertions.assertThrows(NoAvailablePosition.class,
                        () -> parkingLot.park(new Car()));
        Assertions.assertEquals("no available position",noAvailablePosition.getMessage());
    }

}
