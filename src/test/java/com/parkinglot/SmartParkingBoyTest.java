package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoyTest {

    @Test
    void should_return_car_when_fetch_given_a_parking_lot_and_a_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);

        //when
        Car fetchedCar = parkingBoy.fetch(ticket);

        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_car_twice_when_fetch_given_a_parking_lot_and_tickets() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);
        //when
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1,fetchedCar1);
        Assertions.assertEquals(car2,fetchedCar2);
    }
    @Test
    void should_throw_exception_when_fetch_given_a_parking_lot_and_a_wrong_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();
        parkingBoy.park(car);

        //when
        //then
        UnrecognizedTicketException unrecognizedTicketException  =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                () -> parkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket",unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_a_parking_lot_and_a_used_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);

        //when
        UnrecognizedTicketException unrecognizedTicketException  =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> parkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket",unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_when_park_given_a_parking_lot_and_a_car_and_no_position() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(2));
        parkingLots.add(new ParkingLot(2));
        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLots);

        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());

        //when
        NoAvailablePosition noAvailablePosition  =
                Assertions.assertThrows(NoAvailablePosition.class,
                        () -> parkingBoy.park(new Car()));
        Assertions.assertEquals("no available position",noAvailablePosition.getMessage());
    }

    @Test
    void should_park_second_park_when_park_given_a_parking_lot_and_a_car_and_first_position_small_second_position() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();

        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);

        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        parkingLot1.park(new Car());


        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLots);
        parkingBoy.park(new Car());

        //when
        Ticket ticket = parkingBoy.park(new Car());


        //then
        Assertions.assertTrue(()->parkingLot2.isFetch(ticket));

    }

}
